name := "SparkTutorial1"

version := "0.1"

scalaVersion := "2.11.12"

val sparkVersion = "2.3.0"
val slf4jVersion = "1.7.5"

// Spark
libraryDependencies ++= Seq(
  "org.apache.spark" %% "spark-core" % sparkVersion withSources(),
  "org.apache.spark" %% "spark-sql" % sparkVersion withSources(),
  "org.apache.spark" %% "spark-streaming" % sparkVersion withSources(),
  "org.apache.spark" %% "spark-streaming-kafka-0-10" % sparkVersion withSources()
)
// Logs
libraryDependencies ++= Seq("org.slf4j" % "slf4j-api" % slf4jVersion,
  "org.slf4j" % "slf4j-simple" % slf4jVersion)