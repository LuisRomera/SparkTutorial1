import org.slf4j.{LoggerFactory}

object Init {


  val log = LoggerFactory.getLogger(Init.getClass)

  def main(args: Array[String]): Unit = {

    log.info("----------------------------Iniciando Aplicación----------------------------")

  }
}
